import sys
from isa import *

str2opcode = {
    "data": Opcode.DATA,
    "rd": Opcode.RD,
    "wr": Opcode.WR,
    "wrn": Opcode.WRN,
    "nop": Opcode.NOP,
    "mov": Opcode.MOV,
    "add": Opcode.ADD,
    "sub": Opcode.SUB,
    "mul": Opcode.MUL,
    "div": Opcode.DIV,
    "mod": Opcode.MOD,
    "cmp": Opcode.CMP,
    "jmp": Opcode.JMP,
    "jg": Opcode.JG,
    "jl": Opcode.JL,
    "je": Opcode.JE,
    "jng": Opcode.JNG,
    "jnl": Opcode.JNL,
    "jne": Opcode.JNE,
    "sw": Opcode.SW,
    "rdm": Opcode.RDM,
    "hlt": Opcode.HLT,
}

registers = {
    "%reg0": "%reg0",
    "%reg1": "%reg1",
    "%reg2": "%reg2",
    "%reg3": "%reg3",
    "%reg4": "%reg4",
    "%reg5": "%reg5",
    "%rax": "%reg0",
    "%rbx": "%reg1",
    "%rcx": "%reg2",
    "%rdx": "%reg3",
    "%rsi": "%reg4",
    "%rdi": "%reg5",
    "%rin": "%reg0",
    "%rou": "%reg5",
}

reg2index = {"%reg0": 0, "%reg1": 1, "%reg2": 2, "%reg3": 3, "%reg4": 4, "%reg5": 5}


def translate(text):
    line_pos = 0
    terms = []
    instr = []
    labels = {}
    variables = {}
    data_section = False

    instr.append("jmp")
    terms.append(Term(line_pos, ".start", 0, 0, 0))
    line_pos += 1

    for line in text.split("\n"):
        tokens = line.split()

        if len(tokens) == 0:
            continue
        if tokens[0][0] == ";":
            continue
        for i in range(len(tokens)):
            tokens[i] = str(tokens[i])
            if tokens[i][0] == ";":
                tokens = tokens[0:i]
                break

        if len(tokens) == 1 and tokens[0][0] == "." and tokens[0][-1] == ":":
            assert tokens[0][0:-1] not in labels.keys(), "Label already exists!"
            labels[tokens[0][0:-1]] = line_pos

            data_section = bool(tokens[0] == ".data:")
            continue

        if data_section:
            assert len(tokens) >= 2, "Wrong format!"
            assert tokens[0][0] != "%", "Registers can't be accessed in data section"
            assert tokens[0] not in variables.keys(), "Variable already exists!"
            variables[tokens[0]] = line_pos
            instr.append("data")

            temp_str = ""
            if len(tokens) > 1:
                temp_str = " ".join(tokens[1:])
            if temp_str[0] == '"' and temp_str[-1] == '"':
                for sym in temp_str[1:-1]:
                    instr.append("data")
                    terms.append(Term(line_pos, ord(sym), 0, AddrMode.LIT.value, 0))
                    line_pos += 1
                terms.append(Term(line_pos, 0, 0, AddrMode.LIT.value, 0))
            else:
                terms.append(Term(line_pos, tokens[1], 0, AddrMode.LIT.value, 0))
        else:
            assert tokens[0] in str2opcode.keys(), "Unknown instruction: {}".format(tokens[0])
            assert len(tokens) - 1 == args_count[tokens[0]], "Wrong number of arguments!"
            instr.append(tokens[0])

            while len(tokens) < 3:
                tokens.append(0)

            token1 = tokens[1]
            if isinstance(tokens[1], str):
                token1 = tokens[1].replace("(", "").replace(")", "")
            if tokens[1] in registers.keys():
                tokens[1] = registers[tokens[1]]
            if tokens[2] in registers.keys():
                tokens[2] = registers[tokens[2]]
            assert args_count[tokens[0]] < 2 or (
                args_count[tokens[0]] == 2 and (token1 in variables.keys() or token1 in registers.keys())
            ), "First operand must be a variable or a register"
            terms.append(Term(line_pos, tokens[1], tokens[2], 0, 0))

        line_pos += 1

    assert ".start" in labels.keys(), "Start label not found"

    code = []

    for i in range(len(terms)):
        is_indirect = False
        arg1 = terms[i].arg1
        arg2 = terms[i].arg2

        if isinstance(arg1, str) and arg1[0] == "(" and arg1[-1] == ")":
            is_indirect = True
            arg1 = arg1[1:-1]

        if arg1 in labels.keys():
            terms[i] = Term(terms[i].line, labels[arg1], arg2, AddrMode.PTR, 0)
        elif arg1 in variables.keys():
            terms[i] = Term(terms[i].line, variables[arg1], arg2, AddrMode.IND if is_indirect else AddrMode.PTR, 0)
        elif arg1 in registers.keys():
            terms[i] = Term(terms[i].line, reg2index[arg1], arg2, AddrMode.REG, 0)
        else:
            terms[i] = Term(terms[i].line, int(arg1), arg2, AddrMode.IND if is_indirect else AddrMode.LIT, 0)

        is_indirect = False
        arg1 = terms[i].arg1
        arg2 = terms[i].arg2
        if isinstance(arg2, str) and arg2[0] == "(" and arg2[-1] == ")":
            is_indirect = True
            arg2 = arg2[1:-1]

        if arg2 in labels.keys():
            terms[i] = Term(terms[i].line, arg1, labels[arg2], terms[i].mode1, AddrMode.PTR)
        elif arg2 in variables.keys():
            terms[i] = Term(
                terms[i].line, arg1, variables[arg2], terms[i].mode1, AddrMode.IND if is_indirect else AddrMode.PTR
            )
        elif arg2 in registers.keys():
            terms[i] = Term(terms[i].line, arg1, reg2index[arg2], terms[i].mode1, AddrMode.REG)
        else:
            terms[i] = Term(
                terms[i].line, arg1, int(arg2), terms[i].mode1, AddrMode.IND if is_indirect else AddrMode.LIT
            )

        code.append({"opcode": str2opcode[instr[i]], "term": terms[i]})

    return code


def main(code_src, code_dst):
    with open(code_src, "rt", encoding="utf-8") as file:
        src = file.read()

    code = translate(src)
    print("Source LoC: {} | Code instr: {}".format(len(src.split("\n")), len(code)))
    write_code(code_dst, code)


if __name__ == "__main__":
    assert len(sys.argv) == 3, "Wrong arguments: translator.py <input_file> <target_file>"
    _, source, target = sys.argv
    main(source, target)
