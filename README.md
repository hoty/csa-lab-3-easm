# EAsm. Транслятор и модель

- P33081, Клименко Вячеслав Витальевич
- `asm | risc | neum | hw | instr | struct | stream | port | cstr | prob1 | pipeline`
- Упрощённый вариант

## Язык программирования

**Форма Бэкуса-Наура**
```ebnf
<программа> ::= <строка_программы> | <строка_программы> <программа>
<строка_программы> ::= <метка> | <адресная команда> <операнд> [<операнд>] | 
<безадресная команда> | <метка константы> <константа> | <пустая строка> 

<метка> ::= '.' <слово>
<регистр> ::= '%reg' <число>
<адресная команда> = mov | add | sub | ... | sw | rdm | (см. систему команд)
<безадресная команда> ::= rd | wr | wrn | ... | hlt
<операнд> ::= <число> | <метка> | <регистр>
<константа> ::= <число> | <число> '<слово>'
<слово> ::= <символ> | <слово> <символ>
<число> ::= <цифра> | <число> <цифра>
<цифра> ::= 0 | 1 | 2 | .. | 8 | 9
<символ> ::= a | b | c | ... | z | A | B | C | ... | Z | <цифра>
```

**Пояснение**

Каждая пустая строка программы это одно из нижеперечисленного:

* **адресная команда**
  * указывается название команды и адреса операндов через пробел
* **безадресная команда**
  * указывается название команды
* **константа**
  * указывается название константы и сама константа
  * константа может быть 16-битным знаковым числом
  * константа может быть строкой
* **управляющие метки**
  * `.start` - точка входа в программу, обязательная
  * `.data` - может быть указана до метки .start, указывает на то, что весь код до ближайшей следующей метки является секцией данных, т.е. частью кода, где можно объявлять переменные.


Пример программы A + B = C

```asm
.data:
  var_a 10
  var_b 5
  var_c 0
.start:
  mov %rax var_a
  add %rax var_b
  mov var_c %rax
  hlt
```

**Семантика**

- Видимость данных - глобальная
- Поддерживаются целочисленные литералы, находящиеся в диапазоне от $`-2^{31}`$ до $`2^{31}-1`$
- Поддерживаются строковые литералы, строку необходимо заключать в двойные кавычки
- Код выполняется последовательно
- Программа обязательно должна включать метку `.start:`, указывающую на первую выполняемую инструкцию. Метка не может указывать на константу.
- Операнды находятся на одной строке с командами
- Пустые строки игнорируются
- Любой текст, который расположен в строке после `';'`, означает комментарий
- Обращение к регистру всегда начинается с символа `'%'`, чтобы не перепутать его с простой переменной

Память выделяется статически, при запуске модели.

**Поддерживаемые операции:**
- `rd` - прочитать значение из входного буфера в регистр %rin (%reg0)
- `wr` - вывести значение (код символа) из выходного регистра %rou (%reg5) в выходной буфер
- `wrn` - вывести число из выходного регистра %rou (%reg5) в выходной буфер
- `nop` - ничего не делает
- `mov <arg1> <arg2>` - поместить значение из источника (регистра / переменной / литерала) в точку назначения (регистр / переменную)
- `add <arg1> <arg2>` - прибавить к значению первого аргумента (регистра / переменной) значение второго аргумента (регистра / переменной / литерала)
- `sub <arg1> <arg2>` - отнять от значения первого аргумента (регистра / переменной) значение второго аргумента (регистра / переменной / литерала)
- `mul <arg1> <arg2>` - умножить значение первого аргумента (регистра / переменной) на значение второго аргумента (регистра / переменной / литерала)
- `div <arg1> <arg2>` - разделить значение первого аргумента (регистра / переменной) на значение второго аргумента (регистра / переменной / литерала)
- `mod <arg1> <arg2>` - посчитать остаток от деления значения первого аргумента (регистра / переменной) на значение второго аргумента (регистра / переменной / литерала)
- `cmp <arg1> <arg2>` - сравнить аргументы (регистры / переменные / литералы) и установить флаги 
- `sw <arg1> <arg2>` - сохранить значение из регистра в память
- `rdm <arg1> <arg2>` - прочитать значение из памяти в регистр
- `jmp <arg1>` - перепрыгнуть на метку (переместить IP на адрес метки)
- `jg <arg1>` - перепрыгнуть на метку, если флаги указывают на результат сравнения ">"
- `jl <arg1>` - перепрыгнуть на метку, если флаги указывают на результат сравнения "<"
- `je <arg1>` - перепрыгнуть на метку, если флаги указывают на результат сравнения "="
- `jng <arg1>` - перепрыгнуть на метку, если флаги указывают на результат сравнения "<="
- `jnl <arg1>` - перепрыгнуть на метку, если флаги указывают на результат сравнения ">="
- `jne <arg1>` - перепрыгнуть на метку, если флаги указывают на результат сравнения "!="
- `hlt` - остановить исполнение

## Организация памяти

* Память команд и данных - общая
* Размер машинного слова - `32` бит
* Память реализуется списком словарей, описывающих инструкции / ячейки с данными. Хранится по слову в ячейке.


* Поддерживаются следующие **виды адресаций**:
    * **Прямая**: в качестве аргумента команды передается адрес ячейки, значение в которой будет использовано как операнд.
      Например, если `mem[30] = 25`, то команда `add %rax 30` обозначает, что к значению в регистре добавится число 25.

    * **Косвенная**: в качестве аргумента команды передается адрес, по которому лежит адрес операнда.
      Например, если `mem[30] = 25`, `mem[33] = 30`, то команда `add %rax (33)` также обозначает, что к регистру
      добавится значение 25.


## Система команд

**Регистры:**

- `ip` - счётчик команд:
    - инкрементируется после каждой инструкции или перезаписывается переходом.
- `addr` - адресный регистр:
    - хранит индекс ячейки, в которую можно что-то записать или прочитать
- `buf` - буферный регистр:
    - хранит в себе результат операции
    - в него можно поместить значение любого регистра общего назначения, ячейки памяти или литерала
    - его значение устанавливает флаги
    - его значение может быть передано на вывод
- `%reg0 / %rax / %rin` - регистр общего назначения
    - используется как регистр ввода, данные из буфера ввода попадают туда при вызове соответствующей инструкции
    - может быть использован для хранения данных и арифметических операций
- `%reg1 / %rbx` - регистр общего назначения
    - может быть использован для хранения данных и арифметических операций
- `%reg2 / %rcx` - регистр общего назначения
    - может быть использован для хранения данных и арифметических операций
- `%reg3 / %rdx` - регистр общего назначения
    - может быть использован для хранения данных и арифметических операций
- `%reg4 / %rsi` - регистр общего назначения
    - может быть использован для хранения данных и арифметических операций
- `%reg5 / %rdi / %rou` - регистр общего назначения
    - используется как регистр вывода, данные из него попадают в буферный регистр, а потом в буфер вывода при вызове соответствующей инструкции
    - может быть использован для хранения данных и арифметических операций

**Флаги:**

- `zero` - флаг нулевого значения
- `neg` - флаг отрицательного значения

**Память:**

- Адресуется через регистр `addr`
- Может быть записана с регистра `buf`
- Может быть прочитана в любой регистр общего назначения
- Может передавать значение напрямую на входы АЛУ в качестве операнда (только один операнд может быть взят из памяти за такт т.к. адресный регистр всего один)


**Набор инструкций:**

В качестве аргумента любой инструкции могут быть переданы:
- Литерал - передается напрямую в АЛУ и нигде не сохраняется
- Переменная - на этапе трансляции вместо нее подставляется адрес в памяти, где она будет расположена
- Метка - на этапе трансляции вместо нее подставляется адрес следующей ячейки в памяти
- Регистр общего назначения - при трансляции все имена регистров заменяются их номерами

| Syntax              | Mnemonic              | Кол-во тактов | Comment                                                                                      |
|:--------------------|:----------------------|---------------|:---------------------------------------------------------------------------------------------|
| `rd`                | rd                    | 1             | Читает в регистр %rin                                                                        |
| `wr`                | wr                    | 2             | Записывает из регистра %rou                                                                  |
| `wrn`               | wrn                   | 2             | Записывает из регистра %rou                                                                  |
| `nop`               | nop                   | 1             | -                                                                                            |
| `mov <arg1> <arg2>` | mov `<arg1>` `<arg2>` | 2-4           | -                                                                                            |
| `add <arg1> <arg2>` | add `<arg1>` `<arg2>` | 2-4           | -                                                                                            |
| `sub <arg1> <arg2>` | sub `<arg1>` `<arg2>` | 2-4           | -                                                                                            |
| `mul <arg1> <arg2>` | mul `<arg1>` `<arg2>` | 2-4           | -                                                                                            |
| `div <arg1> <arg2>` | div `<arg1>` `<arg2>` | 2-4           | -                                                                                            |
| `mod <arg1> <arg2>` | mod `<arg1>` `<arg2>` | 2-4           | -                                                                                            |
| `cmp <arg1> <arg2>` | cmp `<arg1>` `<arg2>` | 2-4           | Выполняет sub с установкой флагов, но не сохраняет результат никуда кроме буферного регистра |
| `sw  <arg1> <arg2>` | sw  `<arg1>` `<arg2>` | 2             | Записывает в память из регистра `<arg2>`                                                     |
| `rdm <arg1> <arg2>` | rdm `<arg1>` `<arg2>` | 2             | Читает из памяти в регистр `<arg2>`                                                          |
| `jmp <arg>`         | jmp `<arg>`           | 1             | -                                                                                            |
| `jg <arg>`          | jg `<arg>`            | 1             | -                                                                                            |
| `jl <arg>`          | jl `<arg>`            | 1             | -                                                                                            |
| `je <arg>`          | je `<arg>`            | 1             | -                                                                                            |
| `jng <arg>`         | jng `<arg>`           | 1             | -                                                                                            |
| `jnl <arg>`         | jnl `<arg>`           | 1             | -                                                                                            |
| `jne <arg>`         | jne `<arg>`           | 1             | -                                                                                            |
| `hlt`               | hlt                   | 0             | -                                                                                            |


**Кодирование инструкций:**

- Машинный код сериализуется в список JSON.
- Один элемент списка - одна инструкция.
- Индекс списка - адрес инструкции. Используется для команд перехода.

Пример инструкции:
```json
[
    {
        "opcode": "cmp",
        "term": [
            3,
            5,
            0,
            "register",
            "literal"
        ]
    }
]
```
- `opcode` - строка с кодом операции;
- `term` - информация об инструкции (номер, аргументы, режимы)

Типы данных в модуле [isa](./isa.py), где:
- `Opcode` -- перечисление кодов операций
- `AddrMode` -- перечисление типов адресации
- `Term` -- структура для описания информации об инструкции.

## Транслятор

Интерфейс командной строки: `translator.py <input_file> <target_file>`

Реализовано в модуле: [translator](./translator.py)

Этапы трансляции (функция `translate`):

1. Из кода удаляются комментарии
2. Проверяется общее количество аргументов команд (не более 2)
3. Текст преобразуется в набор термов, метки и переменные сохраняются отдельно в словарях
4. Имена регистров преобразуются к стандартному виду (%reg#)
5. Проверяется наличие метки `.start`
6. В термах раскрываются типы адресации, вместо имен переменных и регистров подставляются адреса

Правила генерации машинного кода:
- один терм - одна инструкция
- вместо переменных и регистров подставляются их адреса, а вместо меток - адреса следующих ячеек в памяти

## Модель процессора

Интерфейс командной строки: `machine.py <machine_code_file> <input_file>`

Реализовано в модуле: [machine](./machine.py)

[Открыть в полный размер](./schema.jpg)

![Schema](./schema.jpg)

### DataPath

Реализован в классе `DataPath`.

Сигналы (обрабатываются за один такт, реализованы в виде методов класса):

- `read_l_reg` - получить значение какого-то регистра общего назначение на левой дорожке блока регистров (выполняется за 1 такт одновременно с `read_r_reg`)
- `read_r_reg` - получить значение какого-то регистра общего назначение на правой дорожке блока регистров (выполняется за 1 такт одновременно c `read_l_reg`)
- `latch_reg` - защелкнуть значение регистра общего назначения (номер регистра выбирается через управляющую дорожку w_sel)
- `latch_addr` - защелкнуть значение в `addr`
- `latch_buf` - защёлкнуть значение в `buf`
- `rd` - прочитать значение из памяти по адресу `addr`
- `wr` - записать значение из `buf` в память по адресу `addr`
- `select_left` - выбрать источник левого операнда для передачи в АЛУ
- `select_right` - выбрать источник правого операнда для передачи в АЛУ
- `calculate (operation)` - выбрать операцию для АЛУ

Флаги:
- `flag_zero` - отражает наличие нулевого значения в `buf`
- `flag_neg` - отражает наличие отрицательного значения в `buf`

### ControlUnit

Реализован в классе `ControlUnit`.

- Hardwired (реализовано полностью на python).
- Моделирование на уровне инструкций.
- Трансляция инструкции в последовательность сигналов: `decode_and_execute_instruction`.

Сигналы:
- `latch_ip` - защелкнуть счётчик команд в ControlUnit

Особенности работы модели:

- Для журнала состояний процессора используется стандартный модуль logging
- Размер памяти, выделенной для машины, задается константой, которую можно менять
- Количество инструкций для моделирования ограничено константой, которую можно менять
- Остановка моделирования осуществляется при помощи исключения `StopIteration` (его порождает инструкция `hlt`).
- Управление симуляцией реализовано в функции `simulation`.

## Тестирование

Реализованные програмы

1. [hello world](examples/easm/hello.easm): вывести на экран строку `'Hello World!'`
2. [cat](examples/easm/cat.easm): программа `cat`, повторяем ввод на выводе.
3. [hellouser](examples/easm/hellouser.easm) -- программа `hello_username`: запросить у пользователя его
   имя, считать его, вывести на экран приветствие
4. [prob1](examples/easm/prob1.easm): найти сумму всех чисел, кратных 3 и 5, не превышающих `1000`.

Интеграционные тесты реализованы тут [integration_test](./integration_test.py):

- через golden tests, конфигурация которых лежит в папке [golden](./golden)


CI:

``` yaml
lab3-easm:
  stage: test
  image:
    name: ryukzak/python-tools
    entrypoint: [""]
  script:
    - poetry install
    - coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t coverage report
    - ruff format --check .
```

где:

- `ryukzak/python-tools` -- docker образ, который содержит все необходимые для проверки утилиты.
- `poetry` -- управления зависимостями для языка программирования Python.
- `coverage` -- формирование отчёта об уровне покрытия исходного кода.
- `pytest` -- утилита для запуска тестов.
- `ruff` -- утилита для форматирования и проверки стиля кодирования.

Пример использования для моего языка:

```shell
$ ./translator.py examples/cat.easm target.out
$ ./machine.py target.out examples/input/input.txt
```

Выводится листинг всех регистров.

- Значения всех регистров выводятся в десятичном формате
- Если в какой-то регистр записан символ, в листинге выводится его код

Также в лог выводятся события ввода/вывода: `INPUT symbol`/`OUTPUT symbol`

Пример для `cat`:

```shell
PS C:\Users\IIec1\PycharmProjects\csa-lab-3-easm> cat examples/input/input.txt
Vyacheslav
PS C:\Users\IIec1\PycharmProjects\csa-lab-3-easm> cat examples/easm/cat.easm
.start:
    rd
    mov %rou %rin
    cmp %rou 0
    je .exit
    wr

    hlt
PS C:\Users\IIec1\PycharmProjects\csa-lab-3-easm> python ./translator.py examples/easm/cat.easm target.out
Source LoC: 10 | Code instr: 8
PS C:\Users\IIec1\PycharmProjects\csa-lab-3-easm> cat target.out
[
  {
    "opcode": "jmp",
    "term": [
      0,
      1,
      0,
      "pointer",
      "literal"
    ]
  },
  {
    "opcode": "rd",
    "term": [
      1,
      0,
      0,
      "literal",
      "literal"
    ]
  },
  {
    "opcode": "mov",
    "term": [
      2,
      5,
      0,
      "register",
      "register"
    ]
  },
  {
    "opcode": "cmp",
    "term": [
      3,
      5,
      0,
      "register",
      "literal"
    ]
  },
  {
    "opcode": "je",
    "term": [
      4,
      7,
      0,
      "pointer",
      "literal"
    ]
  },
  {
    "opcode": "wr",
    "term": [
      5,
      0,
      0,
      "literal",
      "literal"
    ]
  },
  {
    "opcode": "jmp",
    "term": [
      6,
      1,
      0,
      "pointer",
      "literal"
    ]
  },
  {
    "opcode": "hlt",
    "term": [
      7,
      0,
      0,
      "literal",
    ]
  }
]
PS C:\Users\IIec1\PycharmProjects\csa-lab-3-easm> python ./machine.py target.out examples/input/input.txt
INFO:root:TICK: 0 | IP: 0 | ADDR: 0 | BUFF: 0 | RAX: 0 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 0 | N: False | Z: True
INFO:root:TICK: 1 | IP: 1 | ADDR: 0 | BUFF: 0 | RAX: 0 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 0 | N: False | Z: True
INFO:root:INPUT: '86'
INFO:root:TICK: 2 | IP: 2 | ADDR: 0 | BUFF: 0 | RAX: 86 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 0 | N: False | Z: True
INFO:root:TICK: 4 | IP: 3 | ADDR: 0 | BUFF: 86 | RAX: 86 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 86 | N: False | Z: False
INFO:root:TICK: 6 | IP: 4 | ADDR: 0 | BUFF: 86 | RAX: 86 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 86 | N: False | Z: False
INFO:root:TICK: 7 | IP: 5 | ADDR: 0 | BUFF: 86 | RAX: 86 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 86 | N: False | Z: False
INFO:root:OUTPUT: 'V'
INFO:root:TICK: 9 | IP: 6 | ADDR: 0 | BUFF: 86 | RAX: 86 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 86 | N: False | Z: False
INFO:root:TICK: 10 | IP: 1 | ADDR: 0 | BUFF: 86 | RAX: 86 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 86 | N: False | Z: False
INFO:root:INPUT: '121'
INFO:root:TICK: 11 | IP: 2 | ADDR: 0 | BUFF: 86 | RAX: 121 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 86 | N: False | Z: False
INFO:root:TICK: 13 | IP: 3 | ADDR: 0 | BUFF: 121 | RAX: 121 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 121 | N: False | Z: False
INFO:root:TICK: 15 | IP: 4 | ADDR: 0 | BUFF: 121 | RAX: 121 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 121 | N: False | Z: False
INFO:root:TICK: 16 | IP: 5 | ADDR: 0 | BUFF: 121 | RAX: 121 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 121 | N: False | Z: False
INFO:root:OUTPUT: 'y'
INFO:root:TICK: 18 | IP: 6 | ADDR: 0 | BUFF: 121 | RAX: 121 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 121 | N: False | Z: False
INFO:root:TICK: 19 | IP: 1 | ADDR: 0 | BUFF: 121 | RAX: 121 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 121 | N: False | Z: False
INFO:root:INPUT: '97'
INFO:root:TICK: 20 | IP: 2 | ADDR: 0 | BUFF: 121 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 121 | N: False | Z: False
INFO:root:TICK: 22 | IP: 3 | ADDR: 0 | BUFF: 97 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:TICK: 24 | IP: 4 | ADDR: 0 | BUFF: 97 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:TICK: 25 | IP: 5 | ADDR: 0 | BUFF: 97 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:OUTPUT: 'a'
INFO:root:TICK: 27 | IP: 6 | ADDR: 0 | BUFF: 97 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:TICK: 28 | IP: 1 | ADDR: 0 | BUFF: 97 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:INPUT: '99'
INFO:root:TICK: 29 | IP: 2 | ADDR: 0 | BUFF: 97 | RAX: 99 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:TICK: 31 | IP: 3 | ADDR: 0 | BUFF: 99 | RAX: 99 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 99 | N: False | Z: False
INFO:root:TICK: 33 | IP: 4 | ADDR: 0 | BUFF: 99 | RAX: 99 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 99 | N: False | Z: False
INFO:root:TICK: 34 | IP: 5 | ADDR: 0 | BUFF: 99 | RAX: 99 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 99 | N: False | Z: False
INFO:root:OUTPUT: 'c'
INFO:root:TICK: 36 | IP: 6 | ADDR: 0 | BUFF: 99 | RAX: 99 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 99 | N: False | Z: False
INFO:root:TICK: 37 | IP: 1 | ADDR: 0 | BUFF: 99 | RAX: 99 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 99 | N: False | Z: False
INFO:root:INPUT: '104'
INFO:root:TICK: 38 | IP: 2 | ADDR: 0 | BUFF: 99 | RAX: 104 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 99 | N: False | Z: False
INFO:root:TICK: 40 | IP: 3 | ADDR: 0 | BUFF: 104 | RAX: 104 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 104 | N: False | Z: False
INFO:root:TICK: 42 | IP: 4 | ADDR: 0 | BUFF: 104 | RAX: 104 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 104 | N: False | Z: False
INFO:root:TICK: 43 | IP: 5 | ADDR: 0 | BUFF: 104 | RAX: 104 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 104 | N: False | Z: False
INFO:root:OUTPUT: 'h'
INFO:root:TICK: 45 | IP: 6 | ADDR: 0 | BUFF: 104 | RAX: 104 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 104 | N: False | Z: False
INFO:root:TICK: 46 | IP: 1 | ADDR: 0 | BUFF: 104 | RAX: 104 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 104 | N: False | Z: False
INFO:root:INPUT: '101'
INFO:root:TICK: 47 | IP: 2 | ADDR: 0 | BUFF: 104 | RAX: 101 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 104 | N: False | Z: False
INFO:root:TICK: 49 | IP: 3 | ADDR: 0 | BUFF: 101 | RAX: 101 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 101 | N: False | Z: False
INFO:root:TICK: 51 | IP: 4 | ADDR: 0 | BUFF: 101 | RAX: 101 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 101 | N: False | Z: False
INFO:root:TICK: 52 | IP: 5 | ADDR: 0 | BUFF: 101 | RAX: 101 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 101 | N: False | Z: False
INFO:root:OUTPUT: 'e'
INFO:root:TICK: 54 | IP: 6 | ADDR: 0 | BUFF: 101 | RAX: 101 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 101 | N: False | Z: False
INFO:root:TICK: 55 | IP: 1 | ADDR: 0 | BUFF: 101 | RAX: 101 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 101 | N: False | Z: False
INFO:root:INPUT: '115'
INFO:root:TICK: 56 | IP: 2 | ADDR: 0 | BUFF: 101 | RAX: 115 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 101 | N: False | Z: False
INFO:root:TICK: 58 | IP: 3 | ADDR: 0 | BUFF: 115 | RAX: 115 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 115 | N: False | Z: False
INFO:root:TICK: 60 | IP: 4 | ADDR: 0 | BUFF: 115 | RAX: 115 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 115 | N: False | Z: False
INFO:root:TICK: 61 | IP: 5 | ADDR: 0 | BUFF: 115 | RAX: 115 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 115 | N: False | Z: False
INFO:root:OUTPUT: 's'
INFO:root:TICK: 63 | IP: 6 | ADDR: 0 | BUFF: 115 | RAX: 115 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 115 | N: False | Z: False
INFO:root:TICK: 64 | IP: 1 | ADDR: 0 | BUFF: 115 | RAX: 115 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 115 | N: False | Z: False
INFO:root:INPUT: '108'
INFO:root:TICK: 65 | IP: 2 | ADDR: 0 | BUFF: 115 | RAX: 108 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 115 | N: False | Z: False
INFO:root:TICK: 67 | IP: 3 | ADDR: 0 | BUFF: 108 | RAX: 108 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 108 | N: False | Z: False
INFO:root:TICK: 69 | IP: 4 | ADDR: 0 | BUFF: 108 | RAX: 108 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 108 | N: False | Z: False
INFO:root:TICK: 70 | IP: 5 | ADDR: 0 | BUFF: 108 | RAX: 108 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 108 | N: False | Z: False
INFO:root:OUTPUT: 'l'
INFO:root:TICK: 72 | IP: 6 | ADDR: 0 | BUFF: 108 | RAX: 108 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 108 | N: False | Z: False
INFO:root:TICK: 73 | IP: 1 | ADDR: 0 | BUFF: 108 | RAX: 108 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 108 | N: False | Z: False
INFO:root:INPUT: '97'
INFO:root:TICK: 74 | IP: 2 | ADDR: 0 | BUFF: 108 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 108 | N: False | Z: False
INFO:root:TICK: 76 | IP: 3 | ADDR: 0 | BUFF: 97 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:TICK: 78 | IP: 4 | ADDR: 0 | BUFF: 97 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:TICK: 79 | IP: 5 | ADDR: 0 | BUFF: 97 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:OUTPUT: 'a'
INFO:root:TICK: 81 | IP: 6 | ADDR: 0 | BUFF: 97 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:TICK: 82 | IP: 1 | ADDR: 0 | BUFF: 97 | RAX: 97 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:INPUT: '118'
INFO:root:TICK: 83 | IP: 2 | ADDR: 0 | BUFF: 97 | RAX: 118 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 97 | N: False | Z: False
INFO:root:TICK: 85 | IP: 3 | ADDR: 0 | BUFF: 118 | RAX: 118 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 118 | N: False | Z: False
INFO:root:TICK: 87 | IP: 4 | ADDR: 0 | BUFF: 118 | RAX: 118 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 118 | N: False | Z: False
INFO:root:TICK: 88 | IP: 5 | ADDR: 0 | BUFF: 118 | RAX: 118 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 118 | N: False | Z: False
INFO:root:OUTPUT: 'v'
INFO:root:TICK: 90 | IP: 6 | ADDR: 0 | BUFF: 118 | RAX: 118 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 118 | N: False | Z: False
INFO:root:TICK: 91 | IP: 1 | ADDR: 0 | BUFF: 118 | RAX: 118 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 118 | N: False | Z: False
INFO:root:INPUT: '0'
INFO:root:TICK: 92 | IP: 2 | ADDR: 0 | BUFF: 118 | RAX: 0 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 118 | N: False | Z: False
INFO:root:TICK: 94 | IP: 3 | ADDR: 0 | BUFF: 0 | RAX: 0 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 0 | N: False | Z: True
INFO:root:TICK: 96 | IP: 4 | ADDR: 0 | BUFF: 0 | RAX: 0 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 0 | N: False | Z: True
INFO:root:TICK: 97 | IP: 7 | ADDR: 0 | BUFF: 0 | RAX: 0 | RBX: 0 | RCX: 0 | RDX: 0 | RSI: 0 | RDI: 0 | N: False | Z: True
Iteration stopped by HLT
INFO:root:output_buffer: 'Vyacheslav'

Vyacheslav

executed instructions: 65
ticks: 97
```

Пример проверки исходного кода:

```shell
PS C:\Users\IIec1\PycharmProjects\csa-lab-3-easm> poetry run pytest . -v --update-goldens
=============================================================================== test session starts ===============================================================================
platform win32 -- Python 3.11.0, pytest-8.0.1, pluggy-1.4.0 -- C:\Users\IIec1\PycharmProjects\csa-lab3\.venv\Scripts\python.exe
cachedir: .pytest_cache
rootdir: C:\Users\IIec1\PycharmProjects\csa-lab-3-easm
configfile: pyproject.toml
plugins: golden-0.2.2
collected 4 items

integration_test.py::test_translator_and_machine[golden/cat.yml] PASSED                                                                                                      [ 25%]
integration_test.py::test_translator_and_machine[golden/hello.yml] PASSED                                                                                                    [ 50%]
integration_test.py::test_translator_and_machine[golden/hellouser.yml] PASSED                                                                                                [ 75%]
integration_test.py::test_translator_and_machine[golden/prob1.yml] PASSED                                                                                                    [100%] 

================================================================================ 4 passed in 0.60s ================================================================================ 
PS C:\Users\IIec1\PycharmProjects\csa-lab-3-easm> poetry run ruff format .
4 files left unchanged
```