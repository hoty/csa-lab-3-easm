import logging
import sys
from enum import Enum
from isa import Opcode, AddrMode, Term, read_code


class ALUOperation(int, Enum):
    ADD = 0
    SUB = 1
    MUL = 2
    DIV = 3
    MOV = 4
    MOD = 5


class SelOption(int, Enum):
    INP = 0
    BUF = 1
    MEM = 2
    ARG = 3
    REG = 4


class DataPath:
    def __init__(self, mem_size, input_buff, data):
        assert mem_size > 0, "Memory size should be greater than 0."
        assert len(data) < mem_size, "Not enough memory to store data"
        self.mem_size = mem_size
        self.memory = data + [0] * (mem_size - len(data))

        self.gp_regs = [0] * 6
        self.addr_reg = 0
        self.buff_reg = 0
        self.reg_block_l = 0
        self.reg_block_r = 0

        self.left = 0
        self.right = 0
        self.alu = 0

        self.arg = 0
        self.l_arg = 0
        self.r_arg = 0

        self.input_buff = input_buff
        self.output_buff = []

        self.symbols_max = mem_size - len(data)
        self.symbols_cnt = 0
        self.symbols_begin = len(data)

    def read_l_reg(self, l_sel):
        assert 0 <= l_sel <= 6, "Machine has only 6 registers. (r0 - r5)"
        self.reg_block_l = self.gp_regs[l_sel]

    def read_r_reg(self, r_sel):
        assert 0 <= r_sel <= 6, "Machine has only 6 registers. (r0 - r5)"
        self.reg_block_r = self.gp_regs[r_sel]

    def latch_reg(self, w_sel, sel_src: SelOption):
        assert 0 <= w_sel <= 6, "Machine has only 6 registers. (r0 - r5)"
        if sel_src is SelOption.MEM:
            self.gp_regs[w_sel] = self.memory[self.addr_reg]
        elif sel_src is SelOption.BUF:
            self.gp_regs[w_sel] = self.buff_reg
        elif sel_src is SelOption.INP:
            self.gp_regs[w_sel] = ord(self.input_buff.pop(0)[0])
            logging.info("INPUT: '{}'".format(self.gp_regs[w_sel]))

    def latch_addr(self, sel_addr: SelOption):
        if sel_addr is SelOption.BUF:
            self.addr_reg = self.buff_reg
        elif sel_addr is SelOption.ARG:
            self.addr_reg = self.arg

    def latch_buff(self):
        self.buff_reg = self.alu

    def flag_neg(self):
        return self.buff_reg < 0

    def flag_zero(self):
        return self.buff_reg == 0

    def rd(self):
        return self.memory[self.addr_reg]["term"][1]

    def rdm(self):
        return self.memory[self.addr_reg]

    def wr(self):
        if self.buff_reg >= 0:
            self.buff_reg = self.buff_reg % (2**31)
        elif self.buff_reg < 0:
            self.buff_reg = -(-self.buff_reg % (2**31))

        new_term = Term(
            self.memory[self.addr_reg]["term"][0],
            self.buff_reg,
            self.memory[self.addr_reg]["term"][2],
            self.memory[self.addr_reg]["term"][3],
            self.memory[self.addr_reg]["term"][4],
        )
        self.memory[self.addr_reg] = {"opcode": self.memory[self.addr_reg]["opcode"], "term": new_term}

    def wrm(self):
        self.memory[self.addr_reg] = self.buff_reg

    def select_left(self, l_src: SelOption):
        if l_src == SelOption.MEM:
            self.left = self.rd()
        elif l_src == SelOption.REG:
            self.left = self.reg_block_l
        elif l_src == SelOption.ARG:
            self.left = self.l_arg

    def select_right(self, r_src: SelOption):
        if r_src == SelOption.MEM:
            self.right = self.rd()
        elif r_src == SelOption.REG:
            self.right = self.reg_block_r
        elif r_src == SelOption.ARG:
            self.right = self.r_arg

    def calculate(self, operation: ALUOperation):
        if operation is ALUOperation.ADD:
            self.alu = self.left + self.right
        elif operation is ALUOperation.SUB:
            self.alu = self.left - self.right
        elif operation is ALUOperation.MUL:
            self.alu = self.left * self.right
        elif operation is ALUOperation.DIV:
            self.alu = self.left // self.right
        elif operation is ALUOperation.MOV:
            self.alu = self.left
        elif operation is ALUOperation.MOD:
            self.alu = self.left % self.right

    def output(self, as_char):
        if as_char:
            self.output_buff.append(chr(self.buff_reg))
            logging.info("OUTPUT: '{}'".format(chr(self.buff_reg)))
        else:
            self.output_buff.append(str(self.buff_reg))
            logging.info("OUTPUT: '{}'".format(str(self.buff_reg)))


class ControlUnit:
    def __init__(self, data_path: DataPath):
        self.program = data_path.memory
        self.data_path = data_path
        self.instr_ptr = 0
        self._tick = 0
        self.sym_begin = self.data_path.symbols_begin

    def tick(self):
        self._tick += 1

    def get_tick(self):
        return self._tick

    def latch_ip(self, inc):
        if inc:
            self.instr_ptr += 1
        else:
            instr = self.program[self.instr_ptr]
            self.instr_ptr = instr["term"][1]

    def decode_and_execute_instruction(self):
        instr = self.program[self.instr_ptr]
        opcode = instr["opcode"]

        if opcode is Opcode.HLT:
            raise StopIteration

        if opcode is Opcode.NOP:
            self.latch_ip(True)
            self.tick()

        if opcode is Opcode.RD:
            w_sel = 0
            self.data_path.latch_reg(w_sel, SelOption.INP)
            self.latch_ip(True)
            self.tick()

        if opcode is Opcode.RDM:
            if instr["term"][4] is AddrMode.LIT:
                self.data_path.arg = self.sym_begin + instr["term"][2]
            else:
                self.data_path.read_l_reg(instr["term"][2])
                self.data_path.arg = self.sym_begin + self.data_path.reg_block_l

            self.data_path.latch_addr(SelOption.ARG)
            self.data_path.latch_reg(instr["term"][1], SelOption.MEM)

            self.latch_ip(True)
            self.tick()

        if opcode is Opcode.SW:
            self.data_path.read_l_reg(instr["term"][1])
            self.data_path.select_left(SelOption.REG)
            self.data_path.calculate(ALUOperation.MOV)
            self.data_path.latch_buff()

            if instr["term"][4] is AddrMode.LIT:
                self.data_path.arg = self.sym_begin + instr["term"][2]
            else:
                self.data_path.read_l_reg(instr["term"][2])
                self.data_path.arg = self.sym_begin + self.data_path.reg_block_l

            self.data_path.latch_addr(SelOption.ARG)
            self.data_path.wrm()

            self.latch_ip(True)
            self.tick()

        if opcode is Opcode.WR:
            l_sel = 5
            self.data_path.read_l_reg(l_sel)
            self.data_path.select_left(SelOption.REG)
            self.data_path.calculate(ALUOperation.MOV)
            self.data_path.latch_buff()
            self.tick()

            self.data_path.output(True)
            self.latch_ip(True)
            self.tick()

        if opcode is Opcode.WRN:
            l_sel = 5
            self.data_path.read_l_reg(l_sel)
            self.data_path.select_left(SelOption.REG)
            self.data_path.calculate(ALUOperation.MOV)
            self.data_path.latch_buff()
            self.tick()

            self.data_path.output(False)
            self.latch_ip(True)
            self.tick()

        if opcode is Opcode.JMP:
            self.latch_ip(False)
            self.tick()

        if opcode is Opcode.JE:
            if self.data_path.flag_zero():
                self.latch_ip(False)
            else:
                self.latch_ip(True)
            self.tick()

        if opcode is Opcode.JNE:
            if self.data_path.flag_zero():
                self.latch_ip(True)
            else:
                self.latch_ip(False)

        if opcode is Opcode.JG:
            if self.data_path.flag_zero() or self.data_path.flag_neg():
                self.latch_ip(True)
            else:
                self.latch_ip(False)

        if opcode is Opcode.JL:
            if self.data_path.flag_neg():
                self.latch_ip(False)
            else:
                self.latch_ip(True)
            self.tick()

        if opcode is Opcode.JNG:
            if self.data_path.flag_zero() or self.data_path.flag_neg():
                self.latch_ip(False)
            else:
                self.latch_ip(True)
            self.tick()

        if opcode is Opcode.JNL:
            if self.data_path.flag_neg():
                self.latch_ip(True)
            else:
                self.latch_ip(False)
            self.tick()

        if opcode is Opcode.MOV:
            assert not (instr["term"][3] is AddrMode.LIT), "Can't move to literal!"

            if instr["term"][4] is AddrMode.IND:
                self.data_path.arg = instr["term"][2]
                self.data_path.latch_addr(SelOption.ARG)
                self.data_path.select_left(SelOption.MEM)
                self.tick()
                self.data_path.calculate(ALUOperation.MOV)
                self.data_path.latch_buff()
                self.data_path.latch_addr(SelOption.BUF)
                self.data_path.select_left(SelOption.MEM)
            elif instr["term"][4] is AddrMode.REG:
                self.data_path.read_l_reg(instr["term"][2])
                self.data_path.select_left(SelOption.REG)
            elif instr["term"][4] is AddrMode.PTR:
                self.data_path.arg = instr["term"][2]
                self.data_path.latch_addr(SelOption.ARG)
                self.data_path.select_left(SelOption.MEM)
            elif instr["term"][4] is AddrMode.LIT:
                self.data_path.l_arg = instr["term"][2]
                self.data_path.select_left(SelOption.ARG)

            self.data_path.calculate(ALUOperation.MOV)
            self.data_path.latch_buff()
            self.tick()

            if instr["term"][3] is AddrMode.REG:
                w_sel = instr["term"][1]
                self.data_path.latch_reg(w_sel, SelOption.BUF)
            elif instr["term"][3] is AddrMode.PTR:
                self.data_path.arg = instr["term"][1]
                self.data_path.latch_addr(SelOption.ARG)
                self.data_path.wr()

            self.latch_ip(True)
            self.tick()

        if opcode in {Opcode.ADD, Opcode.SUB, Opcode.MUL, Opcode.DIV, Opcode.CMP, Opcode.MOD}:
            assert not (
                instr["term"][3] is AddrMode.PTR and instr["term"][4] is AddrMode.PTR
            ), "VAR to VAR is not supported"
            assert not (
                instr["term"][3] is AddrMode.LIT and instr["term"][4] is AddrMode.LIT
            ), "LIT to LIT is not supported"

            if instr["term"][3] is AddrMode.IND:
                self.data_path.arg = instr["term"][1]
                self.data_path.latch_addr(SelOption.ARG)
                self.data_path.select_left(SelOption.MEM)
                self.tick()
                self.data_path.calculate(ALUOperation.MOV)
                self.data_path.latch_buff()
                self.data_path.latch_addr(SelOption.BUF)
                self.data_path.select_left(SelOption.MEM)
            if instr["term"][3] is AddrMode.REG:
                self.data_path.read_l_reg(instr["term"][1])
                self.data_path.select_left(SelOption.REG)
            elif instr["term"][3] is AddrMode.PTR:
                self.data_path.arg = instr["term"][1]
                self.data_path.latch_addr(SelOption.ARG)
                self.data_path.select_left(SelOption.MEM)
            elif instr["term"][3] is AddrMode.LIT:
                self.data_path.l_arg = instr["term"][1]
                self.data_path.select_left(SelOption.ARG)

            if instr["term"][4] is AddrMode.REG:
                self.data_path.read_r_reg(instr["term"][2])
                self.data_path.select_right(SelOption.REG)
            elif instr["term"][4] is AddrMode.PTR:
                self.data_path.arg = instr["term"][2]
                self.data_path.latch_addr(SelOption.ARG)
                self.data_path.select_right(SelOption.MEM)
            elif instr["term"][4] is AddrMode.LIT:
                self.data_path.r_arg = instr["term"][2]
                self.data_path.select_right(SelOption.ARG)

            if opcode is Opcode.ADD:
                self.data_path.calculate(ALUOperation.ADD)
            elif opcode in {Opcode.SUB, Opcode.CMP}:
                self.data_path.calculate(ALUOperation.SUB)
            elif opcode is Opcode.MUL:
                self.data_path.calculate(ALUOperation.MUL)
            elif opcode is Opcode.DIV:
                self.data_path.calculate(ALUOperation.DIV)
            elif opcode is Opcode.MOD:
                self.data_path.calculate(ALUOperation.MOD)

            self.data_path.latch_buff()
            self.tick()

            if not (opcode is Opcode.CMP):
                if instr["term"][3] is AddrMode.REG:
                    w_sel = instr["term"][1]
                    self.data_path.latch_reg(w_sel, SelOption.BUF)
                elif instr["term"][3] is AddrMode.PTR:
                    self.data_path.arg = instr["term"][1]
                    self.data_path.latch_addr(SelOption.ARG)
                    self.data_path.wr()

            self.latch_ip(True)
            self.tick()

    def __repr__(self):
        state = (
            "TICK: {} | IP: {} | ADDR: {} | BUFF: {} | RAX: {} | RBX: {} | RCX: {} | "
            "RDX: {} | RSI: {} | RDI: {} | N: {} | Z: {}"
        ).format(
            self._tick,
            self.instr_ptr,
            self.data_path.addr_reg,
            self.data_path.buff_reg,
            self.data_path.gp_regs[0],
            self.data_path.gp_regs[1],
            self.data_path.gp_regs[2],
            self.data_path.gp_regs[3],
            self.data_path.gp_regs[4],
            self.data_path.gp_regs[5],
            self.data_path.flag_neg(),
            self.data_path.flag_zero(),
        )
        return state


def simulation(code, mem_size, input_buff, limit):
    data_path = DataPath(mem_size, input_buff, code)
    control_unit = ControlUnit(data_path)
    instr_cnt = 0

    control_unit.__repr__()
    logging.info("%s", control_unit)
    try:
        while True:
            assert instr_cnt < limit, "limit exceeded"
            control_unit.decode_and_execute_instruction()
            control_unit.__repr__()
            instr_cnt += 1
            logging.info("%s", control_unit)
    except EOFError:
        logging.warning("Input buffer is empty!")
    except StopIteration:
        print("Iteration stopped by HLT")
        pass
    logging.info("output_buffer: {}".format(repr("".join(data_path.output_buff))))
    return "".join(data_path.output_buff), instr_cnt, control_unit.get_tick()


def main(code, input_f):
    code = read_code(code)
    input_buff = []
    with open(input_f, encoding="utf-8") as file:
        input_text = file.read()
        for ch in input_text:
            input_buff.append(ch)
    input_buff.append("\0")

    output, instr_counter, ticks = simulation(code, mem_size=128, input_buff=input_buff, limit=1024)

    print()
    print("".join(output))
    print("\nexecuted instructions:", instr_counter, "\nticks:", ticks)

    return output


if __name__ == "__main__":
    assert len(sys.argv) == 3, "Wrong arguments: machine.py <code_file> <input_file>"
    _, code_file, input_file = sys.argv
    logging.getLogger().setLevel(logging.INFO)
    main(code_file, input_file)
